//
//  Recognize.swift
//  Test1
//
//  Created by Breitenbach's MBP on 8/1/19.
//  Copyright © 2019 Breitenbach's MBP. All rights reserved.
//

import Foundation
import SwiftUI

private let session = AVCaptureSession()

let videoDevice = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .back).devices.first
do {
    deviceInput = try AVCaptureDeviceInput(device: videoDevice!)
} catch {
    print("Could not create video device input: \(error)")
    return
}

session.beginConfiguration()
session.sessionPreset = .vga640x480

guard session.canAddInput(deviceInput) else {
    print("Could not add video device input to the session")
    session.commitConfiguration()
    return
}
session.addInput(deviceInput)
