//
//  CameraButtonView.swift
//  Test1
//
//  Created by Breitenbach's MBP on 8/8/19.
//  Copyright © 2019 Breitenbach's MBP. All rights reserved.
//

import SwiftUI

struct PhotoLibraryView: View {
        @State var showPhotos = false
        
        var body: some View {
            NavigationView {
                Button(action: {self.showPhotos.toggle()}) {
                    VStack{
                        Image(systemName: "photo").resizable().frame(width: 70, height: 50, alignment: .center)
                        Text("Home Improvement Project").fontWeight(.bold)
                        if showPhotos {
                            ImagePicker()
                        }
                        
                    }
                }
            }.navigationBarItems(leading: Text("Hello"))
    
                
            
    }
}

#if DEBUG
struct CameraButtonView_Previews: PreviewProvider {
    static var previews: some View {
        PhotoLibraryView()
    }
}
#endif
