//
//  BackgroundView.swift
//  Test1
//
//  Created by Breitenbach's MBP on 8/19/19.
//  Copyright © 2019 Breitenbach's MBP. All rights reserved.
//

import SwiftUI

struct BackgroundView: View {
    var body: some View {
        ZStack{
            Image("tools")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
        
            
            
        
        }
        
    }
}

#if DEBUG
struct BackgroundView_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundView()
    }
}
#endif
